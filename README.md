Script correspondant aux suppression de fichiers ayant des conditions spécifiques

Utilisation de la commande find



| Left Aligned | Centered | Right Aligned | Left Aligned | Centered | Right Aligned |
| :----------- | :------: | ------------: | :----------- | :------: | ------------: |
| Cell 1       | Cell 2   | Cell 3        | Cell 4       | Cell 5   | Cell 6        |
| Cell 7       | Cell 8   | Cell 9        | Cell 10      | Cell 11  | Cell 12       |


Commande ps



[file2array](https://gitlab.com/jfern/SHELL/-/blob/master/file2array.ksh)
[file2array2](file2array.ksh)
[snipets](https://gitlab.com/-/snippets/2157970)
<!-- leave a blank line here -->
<script src="https://gitlab.com/jfern/SHELL/-/blob/master/file2array.ksh"></script>
<!-- leave a blank line here -->

<script src="https://gitlab.com/-/snippets/2157970"></script>


## Création de fichiers de test vieux
```shell
touch -t [AAMMJJhhmm] fichier 
```
Par défaut, la date de création d’un fichier est l’instant où la commande touch est exécutée.
L’option -t permet de modifier la date de dernière modification du fichier à la date inscrite [AAMMJJhhmm]
```shell
touch -t 1401010000 ~/test1 # fichier test1 créé pour le 1er janvier 2014
touch -t 1402010000 ~/test2 # fichier test2 créé pour le 1er février 2014
touch -t 1403010000 ~/test3 # fichier test3 créé pour le 1er mars 2014
touch -t 1504090000 ~/test4 # fichier test4 créé pour le 15 avril 2015
```
### Références
[Suppression d'un fichier en fonction d'un période](https://webdevpro.net/script-de-bash-pour-supprimer-des-fichiers-et-dossiers-en-fonction-dune-periode/)

