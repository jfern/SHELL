sauvegarde_rm=~/.rm_saved/

function rm {
    local opt_force=0
    local opt_interactive=0
    local opt_recursive=0
    local opt_verbose=0
    local opt_empty=0
    local opt_list=0
    local opt_restore=0
    local opt
}

OPTERR=0
#Analyse des arguments de la ligne de commandes
while getopts ":dfirRvels-:" opt ; do 
    case $opt in
        d) ;; # ignorée
        f) opt_force=1 ;;
        i) opt_interactive=1 ;;
        r|R) opt_recursive=1 ;;
        e) opt_empty=1 ;;
        l) opt_list=1 ;;
        s) opt_restore=1 ;;
        v) opt_verbose=1 ;;
        -) case $OPTARG in
            directory) ;;
            force)          opt_force=1         ;;
            interactive)    opt_interactive=1   ;;
            recursive)      opt_recursive=1     ;;
            verbose)        opt_verbose=1       ;;
            help)           /bin/rm  --help
                            echo 
        