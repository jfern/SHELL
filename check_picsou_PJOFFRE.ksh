#!/usr/bin/env bash



BANDE_AP=$1
REP_TRACE="$HOME/exploit/picsou/chaine_BSCSE-${BANDE_AP}/log"
FIC_CFG=$HOME/exploit/nagios/cfg/check_picsou_PJOFFRE.cfg
OK=0
KO=1
MATCH="Fin de Picsou sans erreur boquante"

# function valeurDe {
	# local valeur
	# variable=$1
#	Récuypérer le troisième  valeur et l'affecter au variable $variable
	# valeur=$(grep "${BANDE_AP}:${variable}" $CFG 2>/dev/null | awk BEGIN {FS=":"} print "$3")
	# eval $variable='$valeur'
	# unset variable
# }


#valeurDe REP_TRACE
echo " REP_TRACE : $REP_TRACE "

FIC_TRACE=$(find $REP_TRACE -name PICSOU_PICSOU-BSCSE_${BANDE_AP}-PJOFFRE_$(date '+%d-%m-%Y')_*.log -mtime -1)
echo " FIC_TRACE : $FIC_TRACE"

if  grep -q "$MATCH" "$FIC_TRACE" 2>/dev/null 
then 
	echo "OK"
	exit $OK 
fi

echo "Le programme $0 s'est arrête avec ERREUR"
exit $KO
