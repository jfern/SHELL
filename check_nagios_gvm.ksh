#!/usr/bin/env ksh
#
#  Script %name:        check_nagios_gvm.sh %
#  Instance:
#  %version:            2 %
#  Description:
#  %created_by:         juaferna %
#  %date_created:       Thu Apr 04 15:46:24 2018 %

#################################################################################
# Attention : Cette version du script destine au Provisioning 3G
# On vérifiera seulement que les process du GMD et du VMD EIR sont bien lancé
#################################################################################
###########################################################################################################
# MAIN check_nagios_gvm.ksh
# 1 On lance le script checkgvsm
# 2 Sur la sortie on scrute le contenu afin de detecter le lignes qui sont en erreur
# 3 Si le comptage est supérieur à 0 alors on sort en erreur
# 4 Sinon Ok
#
###########################################################################################################
#1 Execution de checkgvsm.sh
SHL_HOME="/CIEUAT55/buksa900/bscs/shl"
OK=0
KO=1


export TMP=$HOME/exploit/nagios/tmp
mkdir -p $TMP
NOW=$(date '+%Y%m%d_%H%M')
LOG=$TMP/gvm_$NOW.log

#Comptage des lignes en erreur
$SHL_HOME/checkgvm.sh | grep "absent" | grep -v "vmdeir"  >$LOG 2>/dev/null
COUNT_ABSENT=$(wc -l $LOG | awk '{print $1 }')
[ $COUNT_ABSENT -gt 0 ] && cat $LOG  && exit $KO
echo "GVM OK"
exit $OK


